<?php
/**
 * @file
 * Template file for the progress bar.
 */
?>
<div class="progress">
	<?php if($gauge == true): ?><span class="value-total"><?php echo $total.$unit; ?></span><?php endif; ?>
    <div class="progress-bar <?php if($pourcentage < 5) echo 'push-right' ?>" style="width: <?php echo $pourcentage; ?>%;">
        <span class="sr-only"><?php echo $pourcentage.'%'; ?></span>
        <?php if($gauge == true): ?><span class="value-used"><?php echo $used.$unit; ?></span><?php endif; ?>
    </div>
</div>
