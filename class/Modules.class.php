<?php

class Modules extends CheckFamily{


  public function __construct() {
  		parent::__construct(); 
  		$this->name = 'Modules';

  		$this->table_header = array('Module','Version','Files','State');

		
  }

  public function checkCustomModules() {

    $projects = update_get_projects();

	foreach($projects as $key=>$project) {
		if($project['datestamp'] == 0){
			
			$path = drupal_get_path('module', $key);
			$num_files = count(glob($path.'/*'));

			$check = new Check('');

			$check->getDisplay()->setValues(array($project['info']['name'], $project['info']['version'], $num_files.' files','111'));
			$check->getDisplay()->setType('no_label');

			$project['project_status'] ? $check->setValue('enabled') : $check->setValue('disabled');
			
			$checks[] = $check;
		}
	}

		

	return $checks;
  }
}
