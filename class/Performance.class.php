<?php

class Performance extends CheckFamily{


  public function __construct() {
  		$this->name = 'Performance';
		parent::__construct(); 
  }

	// Cache pages for anonymous users
	public function checkCachePages(){

		$check = new Check('Cache pages for anonymous users');
		$check->setValue(variable_get('cache', 0));

		return $check;
	}

	// Cache blocks
	public function checkCacheBlocks(){

		$check = new Check('Cache blocks');
		$check->setValue(variable_get('block_cache', 0));

		return $check;
	}

	// Cache lifetime
	public function checkCacheLifeTime(){

		$check = new Check('Cache life time');
		$check->setValue(variable_get('cache_lifetime', 0));

		return $check;
	}
	
	// JS optimization
	public function checkJsOptimization(){

		$check = new Check('JS optimization');
		$check->setValue(variable_get('preprocess_js', 0));

		return $check;
	}
	
	// CSS optimization
	public function checkCssOptimization(){

		$check = new Check('CSS optimization');
		$check->setValue(variable_get('preprocess_css', 0));

		return $check;
	}
	
	// PHP APC extension
	public function checkApcExtension(){

		$check = new Check('PHP APC Extension');

    if(extension_loaded('apc') && ini_get('apc.enabled')) $check->setValue(1); 
    else $check->setValue(0); 

		return $check;
	}
	
	
}
