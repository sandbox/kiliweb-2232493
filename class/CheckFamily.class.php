<?php

class CheckFamily{

	protected $name;
	protected $checks;
  protected $table_header;

  public function __construct() {

  	$checks = array();
    $this->table_header = array('Label','Value');

	}

  public function getName() {

    return $this->name;
  }

  public function setName($name) {

  	$this->name = $name;
  }

  public function setChecks($checks) {

		$this->checks = $checks;

  }

  public function getChecks() {

    return $this->checks;
  }

  public function getTableHeader() {

    return $this->table_header;
  }

  public function process(){

    foreach(get_class_methods($this) as $method) {

      if(strpos($method, 'check') === 0) {

        $result = $this->$method();

        if(is_a($result, 'Check')) $checks[] = $result;
        else if(is_array($result)) {
          foreach($result as $value) { 
            if(is_a($value, 'Check')) $checks[] = $value;
          }
        }
        
      }
    }

    $this->checks = $checks;
  }

  public function render() {

    $rows = array();
    foreach($this->getChecks() as $check){
      $rows[] = $check->getDisplay()->render();
    }

    $html = theme('table', array('header' => $this->table_header, 'rows' => $rows));

    return $html;
  }
}
