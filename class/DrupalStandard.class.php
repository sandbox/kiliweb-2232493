<?php

class DrupalStandard extends CheckFamily{


  public function __construct() {
  		$this->name = 'Drupal Standard';
		parent::__construct(); 
  }

		
	// Drupal version
	public function checkDrupalVersion(){
		
		$check = new Check('Drupal Version');
		$check->setValue(VERSION);

		return $check;
	}

  public function checkCountUser() {

  	$check = new Check('Count User');

  	$query = db_select('users', 'u');
		$query->fields('u', array('uid'));
		$query->condition('u.uid', 0,'!=');
		$value = $query->countQuery()->execute()->fetchField();

		$check->setValue($value);

		return $check;
  }

  public function checkCountUserBlocked() {

  	$check = new Check('Count User blocked');

  	$query = db_select('users', 'u');
		$query->fields('u', array('uid'));
		$query->condition('u.uid', 0,'!=');
		$query->condition('u.status', 0,'=');
		$value = $query->countQuery()->execute()->fetchField();

		$check->setValue($value);

		return $check;
  }

  public function checkCountRoles() {

  	$check = new Check('Count Roles');

  	$query = db_select('role', 'r');
		$query->fields('r', array('rid'));
		$query->condition('r.rid',array(1,2),'NOT IN');
		$value = $query->countQuery()->execute()->fetchField();

		$check->setValue($value);

		return $check;
  }

	public function checkCountNode() {

		$check = new Check('Count node');

  	$query = db_select('node', 'n');
		$query->fields('n', array('nid'));
		$query->condition('n.status', 1,'=');
		$value = $query->countQuery()->execute()->fetchField();

		$check->setValue($value);

		return $check;
  }

  public function checkCountNodeUnpublished() {

  	$check = new Check('Count unpublished node');

  	$query = db_select('node', 'n');
		$query->fields('n', array('nid'));
		$query->condition('n.status', 0,'=');
		$value = $query->countQuery()->execute()->fetchField();

		$check->setValue($value);

		return $check;
  }

  public function checkCountNodeContentTypes() {

  	$check = new Check('Count content types');

  	$query = db_select('node_type', 'nt');
		$query->fields('nt', array('type'));
		$query->condition('nt.disabled',0,'=');
		$value = $query->countQuery()->execute()->fetchField();

		$check->setValue($value);

		return $check;
  }

  /*
  public function checkContentTypes() {

  	$query = db_select('node', 'n');
  	$query->leftjoin('node_type', 'nt', 'n.type = nt.type');
  	$query->fields('n', array('type'));
		$query->fields('nt', array('name'));
		$query->addExpression('COUNT(DISTINCT nid)', 'total');
		$query->groupBy('type');
		$query->orderBy('total','DESC');
		$query->condition('nt.disabled',0,'=');
		$result = $query->execute()->fetchAll();

		$checks = array();
		foreach($result as $key=>$value){
				$check = new Check('# number of '.$value->name);
				$check->setValue($value->total);
				$checks[] = $check;
		}

		return $checks;
  }
  */
}
