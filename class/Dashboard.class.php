<?php

class Dashboard extends CheckFamily{


  public function __construct() {
  		$this->name = 'Dashboard';
		parent::__construct(); 
  }

 
  public function checkUserCreationGraph(){

  		$check = new Check('User creation');

      $quant = new quant;
      $quant->id = 'user_creation';  
      $quant->label = t('User creation');  
      $quant->labelsum = TRUE;   
      $quant->table = 'users';   
      $quant->field = 'created';   
      $quant->dataType = 'single';   
      $quant->chartType = 'line'; 

      $check->setDisplay('quant_graph', array('data'=> $quant,'period'=>'-3 year'));

  		return  $check;
  }

  public function checkNodeCreationGraph(){

      $check = new Check('Node creation');

      $quant = new quant;
      $quant->id = 'node_creation';  
      $quant->label = t('Node creation');  
      $quant->labelsum = TRUE;   
      $quant->table = 'node';   
      $quant->field = 'created';  
      $quant->dataType = 'single';   
      $quant->chartType = 'line';  

      $check->setDisplay('quant_graph', array('data'=> $quant,'period'=>'-3 year'));

      return  $check;
  }

  public function checkWatchdogEntriesGraph(){

     
      $check = new Check('Watchdog entries');

        $quant = new quant;
        $quant->id = 'watchdog'; 
        $quant->label = t('Watchdog entries');  
        $quant->labelsum = TRUE;   
        $quant->table = 'watchdog';   
        $quant->field = 'timestamp';   
        $quant->dataType = 'single';  
        $quant->chartType = 'line';  

        $check->setDisplay('quant_graph', array('data'=> $quant,'period'=>'-3 year'));
      
    

      return  $check;
  }

  public function checkNodeCreationByTypeGraph(){

      $check = new Check('Node creation by Type');

      if (module_exists('system_charts')) {
        $chart = system_charts_build('node_counts');
        $check->setDisplay('system_chart', array('data'=> $chart));
      }

      return  $check;
  }

  public function checkUserCreationByRoleGraph(){

      $check = new Check('User creation by role');

      if (module_exists('system_charts')) {
        $chart = system_charts_build('users_per_role');
        $check->setDisplay('system_chart', array('data'=> $chart));
      }

      return  $check;
  }

  public function checkWatchdogEntryByTypeGraph(){

      $check = new Check('checkWatchdogEntryByTypeGraph');

      if (module_exists('system_charts')) {
        $chart = system_charts_build('watchdog_severity');
        $check->setDisplay('system_chart', array('data'=> $chart));
      }

      return  $check;
  }

}
