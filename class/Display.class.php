<?php

class Display {

    private $type;
    private $values;
    private $label;

    function __construct($label = null, $values = null, $type = null) {
        $this->label = $label;
        $this->values = $values;
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getValues() {
        return $this->values;
    }

    public function setValues($values) {
        $this->values = $values;
    }

    public function render() {
        switch($this->type){
            case 'progressbar':
                return array($this->label , theme('audit_report_progressbar', $this->values));
                break;

            case 'quant_graph':
                return array($this->label, theme('audit_report_quant_graph', $this->values));
                break;

            case 'system_chart':
                return array($this->label , theme('audit_report_system_chart', $this->values));
                break;

            case 'no_label':
                return $this->values;
                break;
                
            default:
                // Make sure we always send back an array with 2 or more columns
                if(is_array($this->values)) return array_unshift($this->values, $this->label);
                else return array( $this->label, $this->values);
                break;
        }

        return array();
    }
}
