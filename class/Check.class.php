<?php

module_load_include('php', 'audit_report', 'class/Display.class');

class Check {
    
    protected $name;
    
    private $value;
    private $alert;
    private $display;

    function __construct($name) {
        $this->name = $name;
        $this->display = new Display($name);
    }

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        $this->value = $value;
        // Populate the display, if not yet populated
        if($this->display->getValues() == null) $this->display->setValues($value);
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getDisplay() {
        return $this->display;
    }
}
