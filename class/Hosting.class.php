<?php

module_load_include('php', 'audit_report', 'lib/whois');
module_load_include('php', 'audit_report', 'lib/cpu');
module_load_include('php', 'audit_report', 'lib/distrib');
define('APP_ROOT', drupal_get_path('module', 'audit_report').'/lib/phpsysinfo');
define('PSI_INTERNAL_XML', true);
module_load_include('php', 'audit_report', 'lib/phpsysinfo/config');

class Hosting extends CheckFamily {

  // SimpleXMLElement Object
  // Contain all the information build by PhpSysInfo library
  private $server;

  public function __construct() {
      parent::__construct();
      $this->name = 'Hosting';
      $output = new WebpageXML(true, null);
      $this->server = simplexml_load_string($output->getXMLString());
      // print_r($this->server);
  }

  // @TODO : Test with more provider (OVH / Online)
  public function checkHostingCompany(){

      $check = new Check('Hosting Company');
      $whois = LookupIP($this->checkIpAddress()->getValue());
      $hosting_company = $this->getTextbetween($whois, 'netname:', 'descr:');
      if(isset($details)) $hosting_company .= ' ('.$details.')';
      $check->setValue($hosting_company);
      return $check;
  }

  // Check wether the hosting is shared / dedicated or cloud
  public function checkHostingType(){
    $check = new Check('Hosting type');
    /*if($this->isCloud()) {
      $hosting_type = 'Cloud ('.$this->getCloudType().')';
    }*/
    // if($this->)
  }



  public function checkOS(){
      $check = new Check('OS');
      $check->setValue($this->server->Vitals['OS']->__toString());
      return  $check;
  }

  public function checkDistrib(){
      $check = new Check('Distribution');
      $check->setValue($this->server->Vitals['Distro']->__toString());
      return  $check;
  }

  public function checkDiskSpace(){

      $check = new Check('Storage');

      // Check wich partition is used on the current Drupal site
      // @TODO : check the site/all/files folder, in case of NFS
      $path = explode('/', DRUPAL_ROOT);
      $max_depth = 0;
      $drupal_partition = 0; // by default

      foreach($this->server->FileSystem->Mount as $partition)
      {
        if($partition['MountPoint'] == '/' && $max_depth == 0) $drupal_partition = $partition_id;
        else 
        {
          $mount_point_path = explode('/', $partition['MountPoint']);

          foreach($mount_point_path as $index => $directory)
          {
              if($path[$index] == $directory && $max_depth < $index)
              {
                $drupal_partition = $partition_id;
                $max_depth = $index;
              }
          }
        }
        $partition_id++;
      }

      $total = $this->convertSize($this->server->FileSystem->Mount[$drupal_partition]['Total'], 'Go');
      $free = $this->convertSize($this->server->FileSystem->Mount[$drupal_partition]['Free'], 'Go');
      $used = $this->convertSize($this->server->FileSystem->Mount[$drupal_partition]['Used'], 'Go');

      $check->getDisplay()->setValues(array('used' => $used, 'total' => $total, 'unit' => 'Go'));
      $check->getDisplay()->setType('progressbar');

      $check->setValue($free.'Go '.t('free'));

      return  $check;
  }

  public function checkMemSpace(){

      $check = new Check('Memory');
      $memory = $this->server->Memory;
      $total = round($this->convertSize($memory['Total'], 'Go', 'kB'));
      $used = round($this->convertSize($memory['Used'], 'Go', 'kB'));
      $free = round($this->convertSize($memory['Free'], 'Go', 'kB'));

      $check->getDisplay()->setValues(array('used' => $used, 'total' => $total, 'unit' => 'Go'));
      $check->getDisplay()->setType('progressbar');

      $check->setValue($free.'Go '.t('free'));

      return  $check;
  }

  public function checkCpuLoad(){

      $check = new Check('CPU Load');

      /* get core information (snapshot) */
      $stat1 = GetCoreInformation();
      /* sleep on server for one second */
      sleep(1);
      /* take second snapshot */
      $stat2 = GetCoreInformation();
      /* get the cpu percentage based off two snapshots */
      $data = GetCpuPercentages($stat1, $stat2);
      $sum = 0;
      foreach($data as $cpu_detail){
        foreach($cpu_detail as $key => $value){
          if($key == 'idle') $sum += $value;
        }
      }
      $mean_busy = number_format(100 - $sum / count($data),1);

      $check->getDisplay()->setValues(array('used' => $mean_busy, 'total' => 100, 'unit' => '%', 'gauge' => false), 'progressbar');
      $check->getDisplay()->setType('progressbar');
      
      $check->setValue($mean_busy.'%');

      return  $check;
  }
  
  // Number of process waiting, this should stay under 1 in most cases
  public function checkSystemLoadAvg(){

      $check = new Check('System Load Average');
      $load_avg = end(explode(' ', $this->server->Vitals['LoadAvg']->__toString()));
      $check->setValue($load_avg);

      return  $check;
  }


  public function checkCpu(){

      $check = new Check('Processor');
      $processor = $this->server->Hardware->CPU->CpuCore;
      $threads = count($processor);
      $model = $this->server->Hardware->CPU->CpuCore[0]['Model'];
      $speed = $this->server->Hardware->CPU->CpuCore[0]['CpuSpeed'];
      $cpu_infos = GetCpuInformations();
      $check->setValue($model.' ('.number_format($speed/1000, 2).'GHz / '.$cpu_infos['cores'].' Cores / '.$threads.' Threads)');
        
      return  $check;
    }

  public function checkUptime(){
      $check = new Check('Uptime');
      $uptime = $this->server->Vitals['Uptime']->__toString();
      $time = $this->secondsToTime($uptime);
      $check->setValue($time['d'].' days, '.$time['h'].' hours');
      return  $check;
  }

  // @TODO : manage more complicated RAID configuration
  // Physical Disk 1 /sda
  // Physical Disk 2 /sdb
  // Partions : sda1 sda2 sda3 exactly replicated on sdb1 sdb2 sdb3
  public function checkRaid(){
      $check = new Check('RAID');
      $partition = array();
      foreach($this->server->Plugins->Plugin_MDStatus->Raid as $raid)
      {
        $partition[$raid['Level']->__toString()]++;
      }

      foreach ($partition as $raid_level => $nb)
      {
          $raid_summary[] = $nb.' partitions in '.$raid_level;
      }
      $raid_summary = implode(', ', $raid_summary);

      $check->setValue($raid_summary);
      return  $check;
  }


  public function checkApache() {
      $check = new Check('Web server');
      $check->setValue($this->formatWebServer($_SERVER['SERVER_SOFTWARE']));  

      return $check;
  }

  public function checkPHP() {
  
    $check = new Check('PHP version');
    $check->setValue($this->getShortVersion(phpversion()));
      
    return $check;
  }

  public function checkMysql() {
  
    $check = new Check('MySQL version');
    $check->setValue($this->getShortVersion(Database::getConnection()->version()));
      
    return $check;
  }

  public function checkIpAddress(){

      $check = new Check('IP');
      $check->setValue($this->server->Vitals['IPAddr']->__toString());
      // Alternate solution 
      // $check->setValue($_SERVER['SERVER_ADDR']);

      return  $check;
  }

  private function isCloud(){
      $answer = false;
      if($this->cloudOpenVZ() == true) $answer = 'OpenVZ';
      if($this->cloudXen() == true) $answer = 'Xen';
      // @TODO : we don't have any VMWare environement to test this situation
      // if($this->cloudVMWare() == true) $answer = 'VMWare';

      return $answer;
  }

  /*
  * Shared / Dedicated / Cloud Section
  *
  */

  // Check the server looks like an OpenVZ configuration
  // See more : http://serverfault.com/questions/332848/how-to-verify-if-a-dedicated-server-is-really-dedicated
  private function cloudOpenVZ(){
    
    // Method 1 : IFconfig check
    $data = exec('ifconfig');
    foreach( $data as $line ) {
      if( preg_match('/^venet0/', $line) ) return true;
    }

    // Method 2 : File check user_beancounters
    if(file_exists('/proc/user_beancounters')) return true;

    // Method 3 : File check veinfo
    if(file_exists('/proc/vz/veinfo')) return true;

    // Method 4 : File check /proc/self/status
    $data = file('/proc/self/status');
    foreach( $data as $line ) {
      if( preg_match('/^envID/', $line) ) return true;
    }
    

    return false;
  }

  // Check the server looks like an Xen configuration
  // See more : http://serverfault.com/questions/332848/how-to-verify-if-a-dedicated-server-is-really-dedicated
  private function cloudXen(){

    // Method 1 : Files checks
    if(file_exists('/proc/sys/xen') || file_exists('/sys/bus/xen') || file_exists('/proc/xen')) return true;
   
    // Method 2 : File check /proc/self/status
    $data = file('/proc/self/status');
    foreach( $data as $line ) {
      if( preg_match('/^s_context/', $line) ) return true;
      if( preg_match('/^VxID/', $line) ) return true;
    }

    return false;
  }


  /*
  *  UTILITY Functions
  *  @TODO : put in the main module, or in a trait
  */

  // Take just the interesting part
  private function getShortVersion($version){
      return  substr($version, 0, strpos($version, '-'));
  }

  // Reformat Web Serveur info
  private function formatWebServer($web_server){
      return  str_replace('/', ' ', substr($web_server, 0, strpos($web_server, ' ')));
  }

  private function convertSize($size, $unit_to, $unit_from = 'byte')
  {
      switch($unit_from){
        case 'kB':
        $size = $size*1024;
      }

      switch($unit_to){
        case 'Go':
          return number_format($size/(1024*1024*1024), 0);

        case 'Go':
          return number_format($size/(1024*1024*1024), 0);

        default:
          return $size;
      }
  }

  /** 
   * Convert number of seconds into hours, minutes and seconds 
   * and return an array containing those values 
   * 
   * @param integer $inputSeconds Number of seconds to parse 
   * @return array 
   */ 

  private function secondsToTime($inputSeconds) {

      $secondsInAMinute = 60;
      $secondsInAnHour  = 60 * $secondsInAMinute;
      $secondsInADay    = 24 * $secondsInAnHour;

      // extract days
      $days = floor($inputSeconds / $secondsInADay);

      // extract hours
      $hourSeconds = $inputSeconds % $secondsInADay;
      $hours = floor($hourSeconds / $secondsInAnHour);

      // extract minutes
      $minuteSeconds = $hourSeconds % $secondsInAnHour;
      $minutes = floor($minuteSeconds / $secondsInAMinute);

      // extract the remaining seconds
      $remainingSeconds = $minuteSeconds % $secondsInAMinute;
      $seconds = ceil($remainingSeconds);

      // return the final array
      $obj = array(
          'd' => (int) $days,
          'h' => (int) $hours,
          'm' => (int) $minutes,
          's' => (int) $seconds,
      );
      return $obj;
  }

  private function getTextbetween($src,$start,$end, $offset = 0) {

      $txt=explode($start,$src);
      $txt2=explode($end,$txt[1+$offset]);
      return trim($txt2[0]);
    }


  //
  // Obsolete function No more used
  //
  /*
  private function getSystemMem() {       

    $data = explode("\n", file_get_contents("/proc/meminfo"));
    $meminfo = array();
    foreach ($data as $line) {
      if (strpos($line,':') !== false) {
        list($key, $val) = explode(":", $line);
        $val = array_shift(explode(' ', trim($val)));
          $meminfo[$key] = trim($val);
        }
    }
    return $meminfo;
  }*/

  /*
  private function pingDomain($domain) {

      $starttime = microtime(true);
      // supress error messages with @
      $file      = fsockopen($domain, 80, $errno, $errstr, 10);
      $stoptime  = microtime(true);
      $status    = 0;

      if (!$file){
          $status = -1;  // Site is down
      }
      else{
          fclose($file);
          $status = ($stoptime - $starttime) * 1000;
          $status = floor($status);
      }

      return $status;
  }
  */

  // @TODO : put this code on an external server
  /*
  public function checkResponsTime(){

      $check = new Check('Response Time');

      global $base_url;
      $host = parse_url($base_url, PHP_URL_HOST);
      $time = $this->pingDomain($host);
      $check->setValue($time.' ms');

      return  $check;
  }*/


}
