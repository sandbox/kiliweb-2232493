<?php 

function getLinuxDistro($show_version = true)
{
    //declare Linux distros(extensible list).
    $distros = array(
            "Arch" => "arch-release",
            "Debian" => "debian_version",
            "Fedora" => "fedora-release",
            "Ubuntu" => "lsb-release",
            'Redhat' => 'redhat-release',
            'CentOS' => 'centos-release');
//Get everything from /etc directory.
$etcList = scandir('/etc');

//Loop through /etc results...
$OSDistro;
foreach ($etcList as $entry)
{
    //Loop through list of distros..
    foreach ($distros as $distroReleaseFile)
    {
        //Match was found.
        if ($distroReleaseFile === $entry)
        {
            //Find distros array key(i.e. Distro name) by value(i.e. distro release file)
            $OSDistro = array_search($distroReleaseFile, $distros);
            if($show_version == true) $OSDistro .= ' '.trim(file_get_contents('/etc/'.$distroReleaseFile));

            break 2;//Break inner and outer loop.
        }
    }
}

return $OSDistro;

}