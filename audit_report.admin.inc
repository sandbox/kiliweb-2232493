<?php

/**
 * @file
 * Admin page callbacks for the Audit report module.
 *
 * @ingroup audit_report
 */


/**
 * Form builder; Configure the Audit report settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function audit_report_settings_form($form) {
 
  
  $form['pathauto_max_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum alias length'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => variable_get('pathauto_max_length', 100),
    '#min_value' => 1,
    '#max_value' => $max_length,
    '#description' => t('Maximum length of aliases to generate. 100 is the recommended length. @max is the maximum possible length. See <a href="@pathauto-help">Pathauto help</a> for details.', array('@pathauto-help' => url('admin/help/pathauto'), '@max' => $max_length)),
    '#element_validate' => array('_pathauto_validate_numeric_element'),
  );
  

  $form['pathauto_ignore_words'] = array(
    '#type' => 'textarea',
    '#title' => t('Strings to Remove'),
    '#default_value' => variable_get('pathauto_ignore_words', PATHAUTO_IGNORE_WORDS),
    '#description' => t('Words to strip out of the URL alias, separated by commas. Do not use this to remove punctuation.'),
    '#wysiwyg' => FALSE,
  );



  return system_settings_form($form);
}





