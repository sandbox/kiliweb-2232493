<?php

function audit_report_drupal_standards(){

  $DrupalStandard = new DrupalStandard();
  $DrupalStandard->process();
  
  return $DrupalStandard->render();

}